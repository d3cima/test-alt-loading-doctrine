<?php

namespace App\Doctrine;


class ApiUser
{

    public string $uuid;
    public string $firstname;
    public string $lastname;
}