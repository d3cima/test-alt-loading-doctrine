<?php

namespace App\Doctrine\Types;

use App\Doctrine\ApiUser;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class UserType extends Type
{

    const TYPE = 'user'; // modify to match your type name


    //fix me - put me in a separated service
    // http://emanueleminotto.github.io/blog/service-injection-doctrine-dbal-type
    // Here INJECT cache as well.
    public function loadUser(string $uuid): ApiUser
    {
        $randomUser = json_decode(file_get_contents("https://randomuser.me/api/?seed=" . $uuid));
        $apiUser = new ApiUser();
        $apiUser->uuid = $uuid;
        $apiUser->firstname = $randomUser->results[0]->name->first;
        $apiUser->lastname = $randomUser->results[0]->name->last;

        return $apiUser;
    }

    public function getSQLDeclaration(array $column, AbstractPlatform $platform)
    {
        return $platform->getAsciiStringTypeDeclarationSQL($column);
    }

    public function getName()
    {
        return self::TYPE;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof ApiUser) {
            return $value->uuid;
        }
        throw new \Exception("invalid user");
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return$this->loadUser($value);
    }
}