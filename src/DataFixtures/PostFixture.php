<?php

namespace App\DataFixtures;

use App\Doctrine\ApiUser;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PostFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 10; $i++) {
            $post = new Post();
            $post->setTitle("POST $i");
            $post->setContent("le fameux post magnifique n°$i");

            $post->setAuthor(new ApiUser());
            $post->getAuthor()->uuid = "id-$i";
            $manager->persist($post);
        }
        $manager->flush();
    }
}
